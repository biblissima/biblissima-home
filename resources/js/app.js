document.addEventListener('DOMContentLoaded', function(){
	const resources = {
		'fr' : {
			'key1': 'clé'
		},
		'en' : {
			'key1': 'key1en'
		}
	}
	i18next.init({
		'resources': resources
	})
});