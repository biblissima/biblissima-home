<?php include_once 'locale.php' ?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo _('html_title') ?></title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo _('meta_desc') ?>">
	<!-- Twitter Card -->
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:site" content="Biblissima"/>
  <meta name="twitter:title" content="Biblissima+, observatoire des cultures écrites anciennes de l’argile à l’imprimé"/>
  <meta name="twitter:url" content="https://biblissima.fr"/>
  <meta name="twitter:image" content="https://projet.biblissima.fr/sites/default/files/2021-11/biblissima-baseline-sombre.png"/>
  <meta name="twitter:description" content="<?php echo _('meta_desc') ?>"/>
  <!-- OGP -->
  <meta property="og:type" content="website"/>
  <meta property="og:site_name" content="Biblissima"/>
  <meta property="og:title" content="Biblissima+, observatoire des cultures écrites anciennes de l’argile à l’imprimé"/>
  <meta property="og:url" content="https://biblissima.fr"/>
  <meta property="og:image" content="https://projet.biblissima.fr/sites/default/files/2021-11/biblissima-baseline-sombre.png"/>
  <meta property="og:description" content="<?php echo _('meta_desc') ?>"/>
  <!-- Rich Snippet -->
  <script type="application/ld+json">
	{
	  "@context" : "http://schema.org",
	  "@type" : "WebSite",
	  "name" : "Biblissima",
	  "url" : "https://biblissima.fr",
	  "description" : "<?php echo _('meta_desc') ?>",
	  "screenshot" : "https://biblissima.fr/images/home-biblissima.jpg",
	  "provider" : {
	    "@type" : "Organization",
	    "name" : "ÉquipEx Biblissima+ - Campus Condorcet"
	  },
	  "hasPart": [
	  {
	  	"@type" : "WebSite",
			"name" : "Portail Biblissima",
			"url" : "https://portail.biblissima.fr",
			"description": "Point d'entrée sur le patrimoine écrit du Moyen Âge et de la Renaissance en Occident, du VIIIe au XVIIIe siècle"
		},
		{
	  	"@type" : "WebSite",
			"name" : "IIIF Collections",
			"url" : "https://iiif.biblissima.fr/collections",
			"description": "Moteur de recherche de manuscrits et livres anciens numérisés et interopérables"
		},
		{
	  	"@type" : "WebSite",
			"name" : "Référentiels Biblissima",
			"url" : "https://data.biblissima.fr",
			"description": "Plateforme collaborative de gestion et publication des données d’autorité de Biblissima"
		},
		{
	  	"@type" : "WebSite",
			"name" : "Outils Biblissima",
			"url" : "https://outils.biblissima.fr",
			"description": "Boîte à outils Biblissima d'aide à la lecture et apprentissage des langues anciennes, outils et environnements de travail en XML (catalogage EAD, édition TEI)"
		},
		{
	  	"@type" : "WebSite",
			"name" : "IIIF360",
			"url" : "https://iiif.biblissima.fr/#services-iiif",
			"description": "IIIF360, un service d’expertise autour de IIIF"
		}
		]
	}
	</script>
	<!-- Favicons -->
	<link rel="apple-touch-icon-precomposed" href="https://static.biblissima.fr/images/favicons/apple-touch-icon-152x152.png">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="https://static.biblissima.fr/images/favicons/mstile-144x144.png">
  <link rel="icon" type="image/png" href="https://static.biblissima.fr/images/favicons/favicon.png">
  <!--[if IE]><link rel="shortcut icon" href="https://static.biblissima.fr/images/favicons/favicon.ico"><![endif]-->
  <!-- CSS -->
	<link rel="stylesheet" href="https://static.biblissima.fr/css/common.css">
	<link rel="stylesheet" media="all" href="https://projet.biblissima.fr/libraries/bootstrap-icons-1.5.0/bootstrap-icons.css" />
	<link rel="stylesheet" type="text/css" href="public/css/app.css">
</head>
<body class="home-bbma">
	<?php include 'env.php' ?>
	<?php if (stream_resolve_include_path('menu-bbma.php')) include 'menu-bbma.php'; ?>	
	<header>
		<?php if (stream_resolve_include_path('top-bar.php')) include 'top-bar.php'; ?>
		<div class="banner">
			<div class="site-identity">
      	<span class="navbar-brand">
            <?php echo _('Biblissima+') ?>
    		</a>
        <div class="site-name-slogan">
      		<?php echo _('Biblissima slogan') ?>
    		</div>
  		</div>
		</div>
	</header>
	<section class="introduction jumbotron">
		<div class="container">
			<p class="lead text-center mb-4"><?php echo _('Biblissima introduction') ?></p>
			<p class="text-center mb-0">
				<a href="https://doc.biblissima.fr/contribuer-a-biblissima" class="btn btn-lg btn-info"><?php echo _('Biblissima contribute') ?></a>
				<a href="https://projet.biblissima.fr" class="btn btn-lg btn-info"><?php echo _('Biblissima more about') ?></a>
			</p>
		</div>
	</section>
	<section class="portal">
		<div class="container">
			<h1><?php echo _('Biblissima portal') ?> <span class="triangle-down"></span></h1>
			<div class="row">
				<div class="description col-12 col-md-6">
					<!-- Point d'accès au patrimoine écrit du Moyen &Acirc;ge et de la Renaissance en Occident, du VIII<sup>e</sup> au XVIII<sup>e</sup> siècle -->
					<p class="lead"><strong><?php echo _('Biblissima portal lead') ?></strong></p>
					<!-- <p>Le Portail Biblissima offre un accès unifié à un ensemble de données numériques sur les manuscrits, incunables et imprimés anciens produites par les <a href="https://projet.biblissima.fr/fr/communaute/equipes-fondatrices">partenaires du consortium Biblissima</a>.</p> -->
					<p><?php echo _('Biblissima portal p2') ?></p>
					<p><?php echo _('Biblissima portal p3') ?></p>
					<!-- <p><a href="https://portail.biblissima.fr" class="btn btn-info">Accéder au portail Biblissima</a></p> -->
					<p><a href="https://portail.biblissima.fr" class="btn btn-lg btn-info"><?php echo _('Biblissima portal access') ?></a></p>
				</div>
				<div class="img-container col-12 col-md-6">
					<div class="img-wrapper">
						<a href="https://portail.biblissima.fr" title="Accéder au portail Biblissima">
							<img src="public/images/portal-biblissima.jpg" alt="">
						</a>
						<span class="triangle-angle-top-right"></span>
						<span class="triangle-angle-bottom-left"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="iiifcollections reverse">
		<div class="container">
			<h1>IIIF Collections <span class="triangle-down"></span></h1>
			<div class="row">
				<div class="img-container col-12 col-md-6">
					<div class="img-wrapper">
						<a href="https://iiif.biblissima.fr/collections/" title="Accéder à IIIF Collections">
							<img src="public/images/iiif-collections.jpg" alt="">
						</a>
						<span class="triangle-angle-top-right"></span>
						<span class="triangle-angle-bottom-left"></span>
					</div>
				</div>
				<div class="description col-12 col-md -6">
					<p class="lead"><strong><?php echo _('IIIFCol lead') ?></strong></p>
					<!-- <p>Le moteur <em>IIIF Collections of Manuscripts &amp; Rare Books</em> permet de trouver des manuscrits et imprimés anciens numérisés provenant d’une multitude de bibliothèques numériques compatibles avec les <a href="https://iiif.biblissima.fr">standards <abbr title="International Image Interoperability Framework™">IIIF</abbr></a>.</p> -->
					<p><?php echo _('IIIFCol p1') ?></p>
					<p><?php echo _('IIIFCol p2') ?></p>
					<!-- <p class="text-right"><a href="https://iiif.biblissima.fr/collections/" class="btn btn-info">Accéder à IIIF Collections</a></p> -->
					<p class="text-right"><a href="https://iiif.biblissima.fr/collections/" class="btn btn-lg btn-info"><?php echo _('IIIFCol access') ?></a></p>
				</div>
			</div>
		</div>
	</section>
	<section class="authority">
		<div class="container">
			<h1><?php echo _('Biblissima menu authority file') ?> <span class="triangle-down"></span></h1>
			<div class="row">
				<div class="description col-12 col-md-6">
					<p class="lead"><strong><?php echo _('authority file lead') ?></strong></p>
					<p><?php echo _('authority file p1') ?></p>
					<p><?php echo _('authority file p2') ?></p>
					<!-- <p>Les référentiels sont mis à disposition sous forme de données structurées selon les principes du <em>Linked Open Data</em> et sont exploitables par le biais de services web (<abbr title="Application Programming Interface">API</abbr>).</p> -->
					<p><?php echo _('authority file p3') ?></p>
					<!-- <p><a href="https://data.biblissima.fr/w/Accueil" class="btn btn-info">Accéder aux référentiels Biblissima</a></p> -->
					<p><a href="https://data.biblissima.fr" class="btn btn-lg btn-info"><?php echo _('authority file access') ?></a></p>
				</div>
				<div class="img-container col-12 col-md-6">
					<div class="img-wrapper">
						<a href="https://data.biblissima.fr/w/Accueil" title="Accéder aux référentiels Biblissima">
							<img src="public/images/data-biblissima.jpg" alt="">
						</a>
						<span class="triangle-angle-top-right"></span>
						<span class="triangle-angle-bottom-left"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="tools reverse">
		<div class="container">
			<h1><?php echo _('Biblissima tools') ?> <span class="triangle-down"></span></h1>
			<div class="row">
				<div class="img-container col-12 col-md-6">
					<div class="img-wrapper">
						<a href="https://outils.biblissima.fr" title="Accéder à la Boîte à outils Biblissima">
							<img src="public/images/outils-biblissima.jpg" alt="">
						</a>
						<span class="triangle-angle-top-right"></span>
						<span class="triangle-angle-bottom-left"></span>
					</div>
				</div>
				<div class="description col-12 col-md-6">
					<p class="lead"><strong><?php echo _('Biblissima tools lead') ?></strong></p>
					<p><?php echo _('Biblissima tools p1') ?></p>
						<ul>
							<li><?php echo _('Biblissima tools li1') ?></li>
							<li><?php echo _('Biblissima tools li2') ?></li>
						</ul>
					<!-- <p class="text-right"><a href="https://outils.biblissima.fr" class="btn btn-info">Accéder à la Boîte à outils</a></p> -->
					<p class="text-right"><a href="https://outils.biblissima.fr" class="btn btn-lg btn-info"><?php echo _('Biblissima tools access') ?></a></p>
				</div>
			</div>
		</div>
	</section>
	<section class="iiif">
		<div class="container">
			<h1>IIIF360<span class="triangle-down"></span></h1>
			<div class="row">
				<div class="description col-12 col-md-6">
					<p class="lead"><strong><?php echo _('IIIF360 lead') ?></strong></p>
    			<!-- <p>IIIF (<em>International Image Interoperability Framework™</em>) est un ensemble de standards qui définissent un cadre d’interopérabilité pour la diffusion des images numériques sur le Web.</p> -->
    			<p><?php echo _('IIIF360 p1') ?></p>
    			<p><?php echo _('IIIF360 p2') ?></p>
					<!-- <p><a href="https://iiif.biblissima.fr/#services-iiif" class="btn btn-info">En savoir plus sur IIIF et l'offre IIIF360</a></p> -->
					<p><a href="https://iiif.biblissima.fr/#services-iiif" class="btn btn-lg btn-info"><?php echo _('IIIF360 access') ?></a></p>
				</div>
                <div class="img-container col-12 col-md-6">
					<div class="img-wrapper">
						<a href="https://iiif.biblissima.fr/#services-iiif" title="En savoir plus sur IIIF et l'offre IIIF360">
							<img src="public/images/iiif-logo.jpg" alt="">
						</a>
						<span class="triangle-angle-top-right"></span>
						<span class="triangle-angle-bottom-left"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content-bottom">  
		<div class="container">
			<div class="row">
		      <div class="col-sm-12">
		        <!-- <p>Pour toute demande, veuillez contacter l'équipe Biblissima à l'adresse <strong>team (at) biblissima-condorcet.fr</strong></p> -->
		        <p class="lead"><?php echo _('content-bottom p1') ?></p>
		        <!-- <p class="m-0">Pour toute demande d'accompagnement ou de formation sur les technologies IIIF, vous pouvez contacter <strong>iiif360 (at) listes.campus-condorcet.fr</strong></p> -->
		        <p class="lead m-0"><?php echo _('content bottom p2') ?></p>
		      </div>
			</div>
		</div>
	</section>
	<footer class="footer" role="contentinfo">
    <div class="container">
      <div class="region region-footer">
          <div class="block block-menu block-footer-col-2 block-footer clearfix">
              <ul class="menu nav">
              	<li class="leaf">
                	<a href="https://projet.biblissima.fr/fr/newsletter" target="_blank">Newsletter</a>
                </li>
              	<li class="leaf">
                	<a href="https://projet.biblissima.fr/contact" target="_blank">Contact</a>
                </li>
                <li class="first last leaf">
                	<a href="https://projet.biblissima.fr/mentions-legales" target="_blank"><?php echo _('terms of use') ?></a>
                </li>
                <li class="leaf">
                	<a href="https://projet.biblissima.fr/logos" target="_blank">Logos</a>
                </li>
              </ul>
          </div>
          <div class="block block-menu block-footer-col-2 block-footer clearfix find-us">
            <p><strong><?php echo _('follow us') ?></strong></p>
            <ul class="nav">
              <li><a href="https://twitter.com/biblissima" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://github.com/biblissima" title="Github"><i class="fa fa-github"></i></a></li>
              <li><a href="https://www.youtube.com/channel/UCaHWzwUV0xBAQ-sEdE9EtKg" title="Youtube"><i class="fa fa-youtube-play"></i></a></li>
              <li><a href="https://projet.biblissima.fr/fr/newsletter" title="Newsletter"><i class="fa fa-envelope-o"></i> </a></li>
            </ul>
          </div>
          <div class="block block-block block-footer-col-4 block-footer clearfix">
            <div class="logo-ia">
                <a href="#"></a>
                <p> <?php echo _('ANR desc') ?> </p>
            </div>
          </div>
          <div class="block block-block block-footer-col-2 block-footer clearfix">
          	<p><strong>ÉquipEx BIBLISSIMA+</strong>
                  <br> Campus Condorcet
                  <br> 8 cours des Humanités
                  <br> 93322 Aubervilliers</p>
          </div>
      </div>
    </div>
	</footer>
	<?php if (!empty($piwik)) include 'piwik.php' ?>
</body>
</html>