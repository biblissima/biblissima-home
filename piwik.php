<!-- Matomo -->
<script>
  var _paq = window._paq = window._paq || [];
  _paq.push([function() {
    var self = this;
    /* CNIL requirement for cookies duration */
    function getOriginalVisitorCookieTimeout() {
      var now = new Date(),
      nowTs = Math.round(now.getTime() / 1000),
      visitorInfo = self.getVisitorInfo();
      var createTs = parseInt(visitorInfo[2]);
      var cookieTimeout = 33696000; // 13 mois en secondes
      var originalTimeout = createTs + cookieTimeout - nowTs;
      return originalTimeout;
    }
    this.setVisitorCookieTimeout( getOriginalVisitorCookieTimeout() );
  }]);
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  //_paq.push(["setDomains", ["*.projet.biblissima.fr"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.biblissima.fr/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '8']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//piwik.biblissima.fr/matomo.php?idsite=8&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->
